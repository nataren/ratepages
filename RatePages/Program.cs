﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using MindTouch;
using MindTouch.Dream;

namespace RatePages {
    class Program {
        static void Main(string[] args) {
            var log = LogUtils.CreateLog();
            var stream = new FileStream("pages-keithstage.txt", FileMode.Open);
            var bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            var pages = Encoding.UTF8.GetString(bytes).Split(new string[] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
            pages = pages.Select(page => page.TrimStart().TrimEnd()).OrderByDescending(pp => pp).ToArray();

            // Sometimes reverse the order of the pages
            var orderDecisionMaker = new Random((int)DateTime.Now.Ticks);
            if(orderDecisionMaker.Next() % 2 == 1) {
                pages = pages.Reverse().ToArray();
            }

            // Start rating!
            var random = new Random((int)DateTime.UtcNow.Ticks);
            foreach(var page in pages) {
                var rating =  (random.Next() % 2).ToString();
                var user = "rater" + random.Next() % 11;
                var p = Plug.New(string.Format("{0}/@api/deki", args[0])).WithCredentials(user, args[2]);
                Console.WriteLine("Will post rating '{0}' to page '{1}' as '{2}'", rating, page, user);
                try {
                    var resp = p.At("pages", page, "ratings").With("score", rating).Post();
                    if(resp.Status != DreamStatus.Ok) {
                        Console.WriteLine("Failed to post to rating '{1}' to page '{0}'", page, rating);
                    }
                } catch(Exception e) {
                    log.WarnFormat("Failed to rate page '{0}'", page);
                }
                //Thread.Sleep(100);
            }
        }
    }
}
